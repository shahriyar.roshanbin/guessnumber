package com.example.guessnumber;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    public Boolean firstTime = true;
    public Button guessBTN;
    public Button restartBTN;
    public TextView displayTV;
    public EditText guessEDT;
    public int mainNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

    }
    void displayMassage(String sting,String choices){
        displayTV.setText(sting);
        if (choices == "D") {
            displayTV.setBackgroundColor(Color.rgb(216, 255, 250));

        }else if (choices == "W"){
            displayTV.setBackgroundColor(Color.rgb(200, 0, 0));

        }else {
            displayTV.setBackgroundColor(Color.rgb(0, 200, 0));
        }
    }

    public void init() {
        guessBTN = (Button) findViewById(R.id.guessBTN);
        restartBTN = (Button) findViewById(R.id.restartBTN);
        displayTV = (TextView) findViewById(R.id.displayTV);
        guessEDT = (EditText) findViewById(R.id.guessEDT);
        displayMassage("Welcome\n click to start the GAME","D");


        guessBTN.setEnabled(false);
    }

    public void guessBTNClick(View view) {

            try{
                int guessNumber = Integer.parseInt(guessEDT.getText().toString());
            if (guessNumber > 20 || guessNumber < 1) {
                displayTV.setBackgroundColor(Color.rgb(200, 0, 0));
                displayTV.setText("Wrong\n you have too guess between 1-20");
            } else if (guessNumber == mainNumber) {
                guessBTN.setEnabled(false);
                displayMassage("Great\n You won the number was " + mainNumber,"x");


            } else if (guessNumber > mainNumber) {

                displayMassage("Close\n Try to guess a litle LOWER","W");

            } else {

                displayMassage("Close\n Try to guess a litle HIGHER","W");

            }
        guessEDT.setText(null);
        } catch (Exception e){
                displayMassage("you have too enter a number","W");
            }
    }



    public void restartBTNClick(View view) {
        guessEDT.setText(null);
        if (!guessBTN.isEnabled()) {
            guessBTN.setEnabled(true);
            if (firstTime) {
                restartBTN.setText("restart");
                firstTime = false;
            }
        }

        displayMassage("you have to guess a number between 1 to 20","D");
        mainNumber = createRandNumber();
    }

    public int createRandNumber() {
        Random random = new Random();
        int uper = 21;
        int preNum = 0;
        while (preNum < 1) {
            preNum = random.nextInt(uper);

        }
        return preNum;

    }
}
